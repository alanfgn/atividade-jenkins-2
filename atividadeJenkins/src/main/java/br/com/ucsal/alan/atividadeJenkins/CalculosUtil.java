package br.com.ucsal.alan.atividadeJenkins;

public class CalculosUtil {
	
	public static int fatorial(int numero) {
		int aux = 1;		
		for (int i = 1; i <= numero; i++) {
			aux *= i;
		}
		return aux;
	}

}
