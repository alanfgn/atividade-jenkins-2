package br.com.ucsal.alan.atividadeJenkins;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;

public class CalculosUtilTest  extends TestCase {
	
	@Test
	public void testarFatorial() {
		Assert.assertEquals(120, CalculosUtil.fatorial(5));
	}
}
